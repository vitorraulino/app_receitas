package com.example.receita;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void mostraSalgado(View v){
        Intent intent = new Intent(this, mostraSalgado.class);
        startActivity(intent);
    }
    public void mostraDoce(View v){
        Intent intent = new Intent(this, mostraDoce.class);
        startActivity(intent);
    }
}
