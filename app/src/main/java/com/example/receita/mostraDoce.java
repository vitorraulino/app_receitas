package com.example.receita;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class mostraDoce extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostra_doce);
    }
    public void brigadeiroMostra(View v){
        Intent intent = new Intent(this, verReceita.class);
        intent.putExtra("receita", getResources().getString(R.string.brigadeiro_title).trim());
        startActivity(intent);
    }
    public void pudimMostra(View v){
        Intent intent = new Intent(this, verReceita.class);
        intent.putExtra("receita", getResources().getString(R.string.pudim_title).trim());
        startActivity(intent);
    }
}
