package com.example.receita;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class verReceita extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ver_receita);
        Intent i = getIntent();
        Bundle b = i.getExtras();
        String str = b.getString("receita");
        TextView t = findViewById(R.id.titulo_receita);
        t.setText(str);
    }
}
